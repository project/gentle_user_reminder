<?php

namespace Drupal\gentle_user_reminder;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a gentle user reminder entity type.
 */
interface GentleUserReminderInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
