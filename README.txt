CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration

INTRODUCTION
------------

This Gentle User Reminder module provides an ability to set a reminder for a 
logged-in user. 
The user has the option of setting a reminder for one of these four 
timeframes: daily, weekly, monthly, or a specific day. 
Whenever cron runs 
on the site, email containing a reminder will be sent after selecting any 
interval.
The reminder entries will be maintained to allow the user to view his 
existing reminders and delete any that are no longer needed.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) 
  for further information.


Configuration
------------

* The user can set their current reminder configuration at the URL
'/user/%user/reminder', which is accessible via a tab on their user
profile page. Visiting that page they are presented with a form that lets them
configure the reminder as they wish. 

* They can also see their current reminder entries at the URL
'/user/%user/reminder-entries'on same profile page.
