<?php

namespace Drupal\gentle_user_reminder\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\gentle_user_reminder\Entity\GentleUserReminder;
use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\RedirectCommand;

/**
 * Form controller for the gentle user reminder entity edit forms.
 */
class ReminderForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "reminder_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL) {
    $userId = \Drupal::currentUser()->id();
    $form['verticaltabs'] = [
      '#type' => 'vertical_tabs',
    ];
    $form['reminder_set_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Set a Reminder'),
      '#open' => TRUE,
      '#group' => 'verticaltabs',
    ];
    $form['reminder_set_form']["reminderform"] = [
      "#type" => "container",
      "#attributes" => [
        "class" => ["reminder-date-container"],
      ],
      "#prefix" => '<div class="reminder-functionality">',
      "#suffix" => "</div>",
    ];
    $form['reminder_set_form']['reminderform']['reminder_container'] = [
      '#type' => 'fieldset',
    ];
    $form['reminder_set_form']["reminderform"]['reminder_container']["description"] = [
      "#type" => "textarea",
      "#title" => $this->t("Reminder message"),
      "#description" => $this->t('Please add the following note of reminder.'),
      "#attributes" => [
        "class" => ["reminder-message"],
      ],
      "#prefix" => '<div class="nu-reminder-date-message">',
      "#suffix" => '</div><div class="reminder-message-button"></div>',
    ];
    $form['reminder_set_form']["reminderform"]['reminder_container']["send_email"] = [
      "#type" => "textarea",
      "#title" => $this->t("Send Email To"),
      "#description" => $this->t("Enter the email address you wish to use to send a reminder here. Enter the email addresses using comma separation if you wish to send multiple emails."),
      "#attributes" => [
        "class" => ["reminder-email-message"],
      ],
      "#prefix" => '<div class="nu-reminder-email-message">',
      "#suffix" => '</div><div class="reminder-send-button"></div>',
    ];
    $form['reminder_set_form']["reminderform"]['reminder_container']["interval"] = [
      "#type" => "radios",
      "#title" => t("Reminder Interval on"),
      "#description" => $this->t('Schedule the reminder at particular intervals. Select from the choices below.'),
      "#default_value" => "Daily Basis",
      "#options" => [
        "Daily Basis" => "Daily Basis",
        "Weekly Basis" => "Weekly Basis",
        "Monthly Basis" => "Monthly Basis",
        "On Selected Date" => "On Selected Date",
      ],
    ];
    $form['reminder_set_form']["reminderform"]['interval_container']["date"] = [
      "#type" => "date",
      "#states" => [
        "visible" => [
          ':input[name="interval"]' => ['value' => 'On Selected Date'],
        ],
      ],
      "#title" => $this->t("Set an email reminder for:"),
      "#attributes" => [
        "class" => ["reminder-date"],
      ],
      "#prefix" => '<div class="nu-reminder-date">',
      "#suffix" => '</div><div class="reminder-button"></div>',
    ];
    $form['reminder_set_form']["reminderform"]["actions"] = [
      "#type" => "actions",
      "#attributes" => [
        "class" => ["btn-wrapper"],
      ],
    ];
    $form['reminder_set_form']["reminderform"]["actions"]["reminder"] = [
      "#type" => "submit",
      "#value" => $this->t("Set a Reminder"),
      "#attributes" => [
        "class" => ["btn", "reminder-date-btn"],
      ],
      "#ajax" => [
        "callback" => [$this, "reminderFormAjaxSelectorCallback"],
        "event" => "click",
        "progress" => [
          "type" => "throbber",
          "message" => NULL,
        ],
      ],
    ];
    $form['reminder_list'] = [
      '#type' => 'details',
      '#title' => $this->t('Reminder Entries'),
      '#open' => FALSE,
      '#group' => 'verticaltabs',
    ];
    $form['reminder_list']['view'] = [
      '#type' => 'view',
      '#name' => 'reminder_entries',
      '#display_id' => 'page_1',
      '#arguments' => [
        $userId,
      ],
    ];
    $form["#attached"]["library"][] = "core/drupal.dialog.ajax";
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $reminderDesc = $form_state->getValue("description");
    if (!$reminderDesc) {
      $form_state->setErrorByName('description', $this->t('Please add the reminder message.'));
    }
    $sendEmail = trim($form_state->getValue("send_email"));
    if (!$sendEmail) {
      $form_state->setErrorByName('send_email', $this->t('Please fill in email.'));
    }
    elseif (!\Drupal::service('email.validator')->isValid($sendEmail)) {
      $form_state->setErrorByName('send_email', $this->t('Please provide a valid Email address.'));
    }
    $reminderDate = $form_state->getValue("date");
    $interval = $form_state->getValue("interval");
    if ($interval == 'On Selected Date') {
      if (!$reminderDate || $reminderDate == "undefined") {
        $form_state->setErrorByName('date', $this->t('Please set the reminder date.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function reminderFormAjaxSelectorCallback(&$form, &$form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new RemoveCommand('.form-item--error-message'));
    $response->addCommand(new InvokeCommand('.form-textarea', 'removeClass', ['error']));
    $response->addCommand(new InvokeCommand('.form-date', 'removeClass', ['error']));
    if ($form_state->hasAnyErrors()) {
      $errors = $form_state->getErrors();
      foreach ($errors as $key => $value) {
        $key = str_replace('_', '-', $key);
        $elemId = '#edit-' . str_replace('][', '-', $key);
        $elemError = '<div class="form-item--error-message"><b>' . $value . '</b></div>';
        $response->addCommand(new InvokeCommand($elemId, 'addClass', ['error']));
        $response->addCommand(new AfterCommand($elemId, $elemError));
      }
    }
    else {
      $reminderDate = $form_state->getValue("date");
      $reminderDesc = $form_state->getValue("description");
      $userId = \Drupal::currentUser()->id();
      $interval = $form_state->getValue("interval");
      $sendEmail = $form_state->getValue("send_email");
      $emailList = preg_replace('/[\r\n]+/', "\n", $sendEmail);
      $emailIds = explode("\n", $emailList);

      $emailTo = implode(",", $emailIds);

      if (isset($interval) && !empty($interval)) {
        if ($interval == 'Daily Basis') {
          $reminderDate = date("Y-m-d", strtotime("+1 day"));
        }
        elseif ($interval == 'Weekly Basis') {
          $reminderDate = date("Y-m-d", strtotime("+1 week"));
        }
        elseif ($interval == 'Monthly Basis') {
          $reminderDate = date("Y-m-d", strtotime("+1 month"));
        }
        elseif ($interval == 'On Selected Date') {
          if (isset($reminderDate) && !empty($reminderDate)) {
            $reminderDate = $reminderDate;
          }
        }
      }
      if (isset($reminderDesc) && !empty($reminderDesc) && isset($emailTo) && !empty($emailTo)) {
        \Drupal::messenger()->addStatus(t('All done - look out for our reminder email!.<br>You will receive an email with a reminder in your inbox.'));
        $current_path = \Drupal::service('path.current')->getPath();
        $response->addCommand(new RedirectCommand($current_path));
      }
      /* Create the entity. */
      $entity = GentleUserReminder::create([
        "type" => "gentle_user_reminder",
        "date" => $reminderDate ?? '',
        "description" => $reminderDesc ?? '',
        "interval" => $interval ?? '',
        "email" => $emailTo ?? '',
        "status" => 0,
      ]);
      /* Save the entity */
      $entity->save();
    }
    return $response;
  }

}
