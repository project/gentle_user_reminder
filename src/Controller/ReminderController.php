<?php

namespace Drupal\gentle_user_reminder\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines ReminderController class.
 */
class ReminderController extends ControllerBase {

  /**
   * Render a reminder form.
   */
  public function reminderUser($user) {
    $form = \Drupal::formBuilder()->getForm('Drupal\gentle_user_reminder\Form\ReminderForm', $user);
    return $form;
  }

}
