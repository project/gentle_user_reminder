<?php

namespace Drupal\gentle_user_reminder;

use Drupal\Core\Session\AccountInterface;
use Drupal\gentle_user_reminder\Entity\GentleUserReminder;

/**
 * Service that send reminder email on cron.
 */
class ReminderDateIntegration {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructor for current user.
   */
  public function __construct(AccountInterface $currentUser) {

    $this->currentUser = $currentUser;
  }

  /**
   * If want to display data related to current user.
   */
  public function getData() {
    return $this->currentUser->getDisplayName();
  }

  /**
   * Reminder email cron function on specific intervals.
   */
  public function reminderDateCron() {
    $currentDate = date('Y-m-d');
    $query = \Drupal::database()->select('gentle_user_reminder', 'u');
    $query->fields('u', ['id', 'uid', 'status', 'description__value',
      'date', 'email__value', 'interval',
    ]);
    $query->condition('u.date', $currentDate, '=');
    $query->condition('u.status', 0, '=');
    $result = $query->execute()->fetchAll();
    foreach ($result as $res) {
      $entity = GentleUserReminder::load($res->id);
      $date = $res->date;
      $description = $res->description__value;
      $uid = $res->uid;
      $interval = $res->interval;
      $send_email_to = $res->email__value;

      if (isset($interval) && !empty($interval)) {
        if ($interval == 'Daily Basis') {
          $this->reminderDateEmail($description, $send_email_to);
          $reminderDate = date("Y-m-d", strtotime("+1 day"));
          $entity->set('field_date', $reminderDate);
          $entity->save();

        }
        elseif ($interval == 'Weekly Basis') {
          $this->reminderDateEmail($description, $send_email_to);
          $reminderDate = date("Y-m-d", strtotime("+1 week"));
          $entity->set('field_date', $reminderDate);
          $entity->save();

        }
        elseif ($interval == 'Monthly Basis') {
          $this->reminderDateEmail($description, $send_email_to);
          $reminderDate = date("Y-m-d", strtotime("+1 month"));
          $entity->set('field_date', $reminderDate);
          $entity->save();

        }
        elseif ($interval == 'On Selected Date') {
          $this->reminderDateEmail($description, $send_email_to);
          $entity->set('status', 1);
          $entity->save();
        }
      }
    }
  }

  /**
   * Reminder email function to send email.
   */
  public function reminderDateEmail($description, $send_email_to) {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $mailManager = \Drupal::service('plugin.manager.mail');
    $params['subject'] = 'Reminder Email notification';
    $params['message'] = 'Your reminder message: ' . $description;
    $module = 'gentle_user_reminder';
    $key = 'reminder_email';
    $send = TRUE;
    $to = $send_email_to;
    $result = FALSE;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, TRUE);
    if ($result['result'] !== TRUE) {
      $message = 'There was a problem sending your message and it was not sent.';
      \Drupal::messenger()->addStatus($message);
    }
    else {
      $message = 'Your message has been sent.';
      \Drupal::messenger()->addStatus($message);
    }
  }

}
